import datetime
import os


def replace_sharp_to_triple(s):
    if s.startswith('#'):
        s = f'#{s}'
    return s


def read_md(path):
    with open(path, 'rt', encoding='utf8') as f:
        s = f.read()

        lines = s.split('\n')

        lines2 = [replace_sharp_to_triple(line) for line in lines]

        return '\n'.join(lines2) + '\n\n\\pagebreak\n\n'


count = 0


def add_title(title):
    global count
    count = count + 1
    return f'# {count}. {title}\n\n'


def run(cmd):
    print(f'@@run={cmd}')
    # log.info(f'run={cmd}')
    os.system(cmd)


now = datetime.datetime.now().strftime("%Y-%m-%d")

#########################################################################

title = '영상편집시스템(VCut)'
pdf_file_name = f'{title}.pdf'

# now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


header = f'''
---
date: {now}
listings-disable-line-numbers: True
subtitle: v1.0
titlepage: True
CJKmainfont: NanumGothic
toc-own-page: True
toc-title: 목차
title: {title}
author: (주)단순
...
'''

markdowns = [
    header,
    add_title('뮤뮤'),
    read_md('docs/index.md'),
    add_title('클클'),
    read_md('docs/about.md'),
    add_title('서브'),
    read_md('docs/sub1.md'),
    read_md('docs/sub2.md'),
    read_md('docs/about.md'),
]

docs_dir = '../docs'

path_current = os.path.abspath(os.path.dirname(__file__))

docs_dir = os.path.join(path_current, docs_dir)

path_deeplist_latex = os.path.join(path_current, 'deeplists.tex')

pdf_dir = '../build'
os.makedirs(pdf_dir, exist_ok=True)

path_markdown = os.path.join(pdf_dir, '0.md')
path_pdf = os.path.join(pdf_dir, pdf_file_name)

with open(path_markdown, 'wt', encoding='utf8') as f:
    f.write(''.join(markdowns))

# print(f's_heading={self.s_heading}')

if os.path.exists(path_pdf):
    os.remove(path_pdf)

# run(f'open "{path_markdown}"')

# # ! LaTeX Error: Too deeply nested (lists more than 6 levels deep)
# # https://github.com/jgm/pandoc/issues/2922#issuecomment-360201454
# path_deeplist_latex = os.path.abspath(os.path.join(os.path.dirname(__file__), 'deeplists.tex'))
#
run(
    f'pandoc {path_markdown} -o "{path_pdf}" --from markdown+all_symbols_escapable --template eisvogel --listings --pdf-engine=xelatex --toc --resource-path={docs_dir} -V papersize:a4 -H {path_deeplist_latex}')
#
if os.path.exists(path_pdf):
    run('say gogo')
else:
    run('say fuck')
#
#
run(f'open "{path_pdf}"')
