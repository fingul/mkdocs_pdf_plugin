

pandoc 0.md -o 0.pdf --from markdown+all_symbols_escapable --template eisvogel --listings --pdf-engine=xelatex --toc --resource-path=/Users/m/p/vcut/vcutapi/docs -H ./deeplists.tex

pandoc: deeplists.tex: openBinaryFile: does not exist (No such file or directory)

# REQUIREMENT

    brew cask install mactex
    brew install pandoc
    wget https://raw.githubusercontent.com/Wandmalfarbe/pandoc-latex-template/master/eisvogel.tex -O /usr/local/Cellar/pandoc/2.4/share/x86_64-osx-ghc-8.4.4/pandoc-2.4/data/templates/eisvogel.latex

# INSTALL

pip install mkdocs_pdf_plugin

# config

`mkdocs.yml` file

``` yaml
    plugins:
      - search
      - pdf:
          enabled_if_env: PDF
          heading:
            #        title: "사랑의 천사" # title - default : site_name
            #        author: [저작자] # title - default : site_author
            date: "2018.11.06" #- default : blank
            #        subject: "Markdown"
            listings-disable-line-numbers: true
            #        keywords: [Markdown, Example]
            subtitle: ""
            titlepage: true
            CJKmainfont: NanumGothic
            toc-own-page: true
            toc-title: 목차
```

# run

PDF=1 mkdocs build

# REFERENCE

<https://github.com/Wandmalfarbe/pandoc-latex-template>


# INTERNAL DEPLOY

```
pip install -r requirements_dev.txt

rm -rf dist
python setup.py sdist bdist_wheel
twine upload dist/*
```

# INTERNAL USAGE

```
pandoc example.md -o example.pdf --from markdown --template eisvogel --listings --pdf-engine=xelatex --toc
```
