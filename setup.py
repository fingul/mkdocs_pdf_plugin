from setuptools import setup, find_packages

setup(
    name='mkdocs-pdf-plugin',
    version='0.14.0',
    description='An MkDocs plugin to export content pages as PDF files using pandoc.',
    long_description='The pdf-export plugin will export all markdown pages in your MkDocs repository as PDF files'
                     'using pandoc. '
    ,
    keywords='mkdocs pdf plugin',
    url='https://bitbucket.org/fingul/mkdocs_pdf_plugin/',
    author='fingul',
    author_email='fingul@gmail.com',
    license='MIT',
    python_requires='>=3.6',
    # data_files=[('latex', ['latex/deeplists.tex'])],


    package_data={
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.tex'],
    },
    include_package_data=True,

    install_requires=[
        'mkdocs',
    ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'
    ],
    packages=find_packages(),
    entry_points={
        'mkdocs.plugins': [
            'pdf = mkdocs_pdf_plugin.plugin:PdfPlugin'
        ]
    },
)
